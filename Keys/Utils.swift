//
//  Utils.swift
//  Keys
//
//  Created by Gleb Sabirzyanov on 07/06/2020.
//  Copyright © 2020 Gleb Sabirzyanov. All rights reserved.
//

import Foundation

func delay(_ delay: Double = 1, closure: @escaping ()->()) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

private var alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-+,.абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"

func isInAlphabet(_ char: Character) -> Bool {
    return alphabet.contains(char)
}
