//
//  KeysCombination.swift
//  Keys
//
//  Created by Gleb Sabirzyanov on 08/06/2020.
//  Copyright © 2020 Gleb Sabirzyanov. All rights reserved.
//

import Foundation

protocol KeysCombinationDelegate {
    func update()
    func pushToHistory(stillPressed: [PressedKey])
}

class KeysCombination {
    
    fileprivate var keysPressed: [PressedKey] = []
    fileprivate var flagsPressed: [PressedKey] = []
    
    fileprivate let delegate: KeysCombinationDelegate
    
    fileprivate var isHistory = false
    
    var keys: [PressedKey] {
        return flagsPressed + keysPressed
    }
    
    init(delegate: KeysCombinationDelegate, flags: [PressedKey] = []) {
        self.delegate = delegate
        self.flagsPressed = flags
    }
    
    func addKey(value: String, keyCode: UInt16, flag: Bool) {
        let key = PressedKey(value: value, keyCode: keyCode)
        var isPushedToHistory = false
        if flag {
            // if key is a flag (modifier)
            if let i = flagsPressed.firstIndex(where: {$0.value == value}) {
                flagsPressed[i].isPressed = true
            } else {
                if !keysPressed.isEmpty {
                    self.isHistory = true
                    let flags = self.flagsPressed.filter({$0.isPressed == true}) + [key]
                     print("1111", flags, keys)
                    delegate.pushToHistory(stillPressed: flags)
                    isPushedToHistory = true
                } else {
                    flagsPressed.append(key)
                }
            }
            self.delegate.update()
        } else {
            // if key is not a flag (modifier)
            keysPressed.append(key)
            self.delegate.update()
            if self.flagsPressed.count > 0 {
                self.isHistory = true
                let flags = self.flagsPressed.filter({$0.isPressed == true})
                print("2222", flags, keys)
                isPushedToHistory = true
                self.delegate.pushToHistory(stillPressed: flags)
            }
        }
        if self.keysPressed.count >= 8 && self.flagsPressed.count == 0 {
            // this can be used to move long typed lines to history
            // instead of erasing the current line from the start
//            if !isPushedToHistory {
//                let pressed = self.flagsPressed.filter({$0.isPressed == true})
//                self.delegate.pushToHistory(stillPressed: pressed)
//                self.isHistory = true
//            }
            
            // erase current line from the start if it's too long
            if !isPushedToHistory {
                if !self.keysPressed[0].isPressed {
                    self.keysPressed.remove(at: 0)
                    self.delegate.update()
                }
            }
        }
    }
    
    func removeKey(value: String, keyCode: UInt16, flag: Bool) {
        if flag, let i = self.flagsPressed.firstIndex(where: {$0.value == value}) {
            flagsPressed[i].isPressed = false
        } else if !flag, let i = self.keysPressed.firstIndex(where: {$0.keyCode == keyCode && $0.isPressed == true}) {
            keysPressed[i].isPressed = false
        } else {
            return
        }
        
        if flag && keysPressed.count == 0 {
            self.removeKeyAfterTimeout(value: value, keyCode: keyCode, flag: flag)
        } else {
            delay(1.5) {
                self.removeKeyAfterTimeout(value: value, keyCode: keyCode, flag: flag)
            }
        }
    }
    
    func removeKeyAfterTimeout(value: String, keyCode: UInt16, flag: Bool) {
        // don't remove keys from history
        guard !self.isHistory else {
            if (!flag) {
                self.keysPressed.first(where: {$0.keyCode == keyCode})?.isPressed = false
            }
            return
        }
        if flag {
            if let i = self.flagsPressed.firstIndex(where: {$0.value == value}) {
                if !self.flagsPressed[i].isPressed {
                    self.flagsPressed.remove(at: i)
                    self.delegate.update()
                }
            }
        } else {
            if let i = self.keysPressed.firstIndex(where: {$0.keyCode == keyCode}) {
                if !self.keysPressed[i].isPressed {
                    self.keysPressed.remove(at: i)
                    self.delegate.update()
                }
            }
            // if regular was unpressed, move shortcut to history
            if self.keysPressed.isEmpty {
                self.isHistory = true
                self.delegate.pushToHistory(stillPressed: self.flagsPressed.filter({$0.isPressed == true}))
            }
        }
    }
}
