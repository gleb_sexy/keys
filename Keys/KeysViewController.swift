//
//  KeysViewController.swift
//  Keys
//
//  Created by Gleb Sabirzyanov on 19/05/2020.
//  Copyright © 2020 Gleb Sabirzyanov. All rights reserved.
//

import Cocoa

class KeysViewController: NSViewController {

    var keyViews: [KeyView]!
    var keyCombinations: [KeysCombination]!
    
    var loadedFont: CTFont!
    var bgImage: NSImage!
    
    @IBOutlet var stackView: NSStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let delegate = NSApp.delegate as? AppDelegate
        delegate?.setKeysControllerDelegate(self)
        loadFont()
        loadImage()
        
        let firstKey = KeyView(value: "A", font: loadedFont, image: bgImage, isActive: true)
        keyViews = [firstKey]
        
        let horizontalStack = NSStackView(views: [firstKey])
        horizontalStack.orientation = .horizontal
        stackView.addSubview(horizontalStack)
    }
    
    private func loadFont() {
        let fontURL = URL(fileURLWithPath: Bundle.main.resourcePath! + "/Inter-SemiBold.otf")
        let fd = CTFontManagerCreateFontDescriptorsFromURL(fontURL as CFURL) as! [CTFontDescriptor]
        self.loadedFont = CTFontCreateWithFontDescriptor(fd[0], 43.0, nil)
    }
    
    private func loadImage() {
        let url = URL(fileURLWithPath: Bundle.main.resourcePath! + "/key-bg.png")
        do {
            let data = try Data(contentsOf: url)
            bgImage = NSImage(data: data)
        } catch {
            print("Unable to get data")
        }
    }
    
}


extension KeysViewController: KeysControllerDelegate {
   
    func showKeys(all: [KeysCombination]) {
        // remove all key views
        for (_, view) in stackView.subviews.reversed().enumerated() {
            view.removeFromSuperview()
        }
        
        for row in all {
            guard row.keys.count > 0 else {
                continue
            }
            
            let horizontalStack = NSStackView()
            horizontalStack.orientation = .horizontal
            horizontalStack.heightAnchor.constraint(equalToConstant: 64).isActive = true

            // create new key views
            for key in row.keys {
                let newKeyView = KeyView(value: key.value, font: loadedFont, image: bgImage, isActive: key.isPressed)
                newKeyView.widthAnchor.constraint(equalToConstant: 64).isActive = true
                newKeyView.heightAnchor.constraint(equalToConstant: 64).isActive = true
                newKeyView.leftAnchor.constraint(equalTo: horizontalStack.leftAnchor)
                newKeyView.topAnchor.constraint(equalTo: horizontalStack.topAnchor)
                
                horizontalStack.addArrangedSubview(newKeyView)
            }
            
            stackView.addArrangedSubview(horizontalStack)
        }
        
    }
}
