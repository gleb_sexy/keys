//
//  AppDelegate.swift
//  Keys
//
//  Created by Gleb Sabirzyanov on 19/05/2020.
//  Copyright © 2020 Gleb Sabirzyanov. All rights reserved.
//

import Cocoa

func acquirePrivileges() -> Bool {
    let accessEnabled = AXIsProcessTrustedWithOptions(
        [kAXTrustedCheckOptionPrompt.takeUnretainedValue() as String: true] as CFDictionary)
    
    if accessEnabled != true {
        print("You need to enable the keylogger in the System Prefrences")
    }
    return accessEnabled == true;
}


@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    fileprivate var keysController: AllKeysController? = AllKeysController()
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        let enabled = acquirePrivileges();
        print(enabled)
        
        // key down
        NSEvent.addGlobalMonitorForEvents(matching: .keyDown, handler: {(event: NSEvent) in
                self.keysController?.onKeyDown(event: event)
        })
        
        NSEvent.addLocalMonitorForEvents(matching: .keyDown, handler: {(event: NSEvent) in
                self.keysController?.onKeyDown(event: event)
                return event
        })
        
        // key up
        NSEvent.addGlobalMonitorForEvents(matching: .keyUp, handler: {(event: NSEvent) in
            self.keysController?.onKeyUp(event: event)
        })
        
        NSEvent.addLocalMonitorForEvents(matching: .keyUp, handler: {(event: NSEvent) in
            self.keysController?.onKeyUp(event: event)
            return event
        })
        
        // modifier keys
        NSEvent.addGlobalMonitorForEvents(matching: .flagsChanged, handler: {(event: NSEvent) in
            self.keysController?.onFlagsChange(event: event)
        })
        
        NSEvent.addLocalMonitorForEvents(matching: .flagsChanged, handler: {(event: NSEvent) in
            self.keysController?.onFlagsChange(event: event)
            return event
        })
        
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    func setKeysControllerDelegate(_ delegate: KeysControllerDelegate) {
        self.keysController?.setDelegate(delegate)
    }
}
